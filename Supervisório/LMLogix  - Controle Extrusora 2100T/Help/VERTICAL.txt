    ' <OBJECT ID="RMChartX1" WIDTH=100% HEIGHT=100% CLASSID="CLSID:4D814D0F-7D71-4E7E-B51E-2885AD0ED9D7"></OBJECT>
 
    With RMChartX1
        .Reset
        .RMCBackColor =  0
        .RMCStyle =  0
        .RMCWidth = 600
        .RMCHeight = 450
        .RMCBgImage = ""
        .Font = "Arial"
        .RMCToolTipWidth = 0
        .RMCHelpingGridSize = 0
        .RMCHelpingGridColor =  0
        .RMCBitmapColor =  0
        '************** Add Region 1 ***************************** 
        .AddRegion
        With .Region(1)
            .Left = 5
            .Top = 5
            .Width = -5
            .Height = -5
            .Footer = ""
            '************** Add caption to region 1 ******************* 
            .AddCaption
            With .Caption
                .Titel = "Relat�rio de Paradas Operacionais"
                .BackColor =  0
                .TextColor =  0
                .FontSize = 10
                .Bold = -1
            End With 'Caption
            '************** Add grid to region 1 ***************************** 
            .AddGrid
            With .Grid
                .BackColor =  0
                .AsGradient = 0
                .BicolorMode =  0
                .Left = 0
                .Top = 0
                .Width = 0
                .Height = 0
            End With 'Grid
            '************** Add data axis to region 1 ***************************** 
            .AddDataAxis
            With .DataAxis(1)
                .Alignment =  1
                .MinValue = 0
                .MaxValue = 50000
                .TickCount = 11
                .Fontsize = 8
                .TextColor =  0
                .LineColor =  0
                .LineStyle =  0
                .DecimalDigits = 0
                .AxisUnit = ""
                .AxisText = ""
            End With 'DataAxis(1)
            '************** Add label axis to region 1 ***************************** 
            .AddLabelAxis
            With .LabelAxis
                .AxisCount = 1
                .TickCount = 22
                .Alignment =  8
                .Fontsize = 8
                .TextColor =  0
                .TextAlignment =  4
                .LineColor =  0
                .LineStyle =  0
                .AxisText = ""
                .LabelString = "Alinhamento*Alinhamento Prensa*Aquecendo arugo*Chap�u*Falta Cesto*Falta Ferramen" _
                             + "ta*Falta Tarugo*Ferramenta*Limpeza*Limpeza na Torre*MesaCheia*Disco de Limpeza*P" _
                             + "neu Lambreta*Regulagem Faca*Reuni�o*Sacando Tarugo*Trocade Disco*Troca Ferrament" _
                             + "a*Troca de Liga*Stem*Sem Registro*Outro Motivo OP"
            End With 'LabelAxis
            '************** Add Series 1 to region 1 ******************************* 
            .AddBarSeries
            With .BarSeries(1)
                .SeriesType =  1
                .SeriesStyle =  7
                .Lucent = 0
                .Color =  0
                .Horizontal = 0
                .WhichDataAxis = 1
                .ValueLabelOn =  11
                .PointsPerColumn = 1
                .HatchMode =  0
                '****** Set data values ******
                .DataString = 
            End With 'BarSeries(1)
        End With 'Region(1)
        .Draw(TRUE)
    End With 'RMChartX1
